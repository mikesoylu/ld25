package com.mikesoylu.fortia 
{
	import starling.display.Button;
	import starling.textures.Texture;
	
	/**
	 * Extends the starling.display.Button but we must modify it for this to work(private->protected)
	 * @author bms
	 */
	public class fButton extends Button 
	{
		public function fButton(upState:Texture, text:String = "", downState:Texture = null) 
		{
			super(upState, text, downState);
		}
	}
}