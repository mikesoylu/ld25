package com.mikesoylu.fortia 
{
	
	/**
	 * An interface for providing state pattern design
	 * @author bms
	 */
	public interface fMode 
	{
		function init(parent:fState):void;
		function update(dt:Number):void;
		function activate():void;
		function deactivate():void;
	}
}