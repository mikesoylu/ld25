package com.mikesoylu.fortia 
{
	import flash.geom.Rectangle;
	import starling.display.Image;
	import starling.textures.Texture;
	
	/**
	 * like fSprite but can't have children
	 */
	public class fImage extends Image implements fIBasic
	{
		
		public function fImage(texture:Texture)
		{
			super(texture);
		}
		public function update(dt:Number):void
		{
			
		}
		public function destroy():void
		{
			
		}
	}
}