package com.mikesoylu.fortia 
{
	import flash.geom.Rectangle;
	import starling.display.MovieClip;
	import starling.textures.Texture;
	
	/**
	 * Basic animated sprite
	 */
	public class fMovieClip extends MovieClip implements fIBasic 
	{
		public function fMovieClip(textures:Vector.<Texture>, fps:int = 12) 
		{
			super(textures, fps);
		}
		
		public function update(dt:Number):void
		{
			
		}
		
		public function destroy():void
		{
			
		}
	}

}