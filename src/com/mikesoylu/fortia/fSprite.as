package com.mikesoylu.fortia 
{
	import flash.geom.Rectangle;
	import starling.display.Sprite;
	
	/**
	 * basic sprite with update and destroy functions
	 * should be used for sprites with children
	 * NOTE: Doesn't manage children. See fLayer.
	 */
	public class fSprite extends Sprite implements fIBasic
	{
		public function update(dt:Number):void
		{
			
		}
		
		public function destroy():void
		{
			
		}
	}
}