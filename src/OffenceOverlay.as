package
{
	import com.mikesoylu.fortia.*;
	import starling.display.*;
	import starling.events.*;
	
	/**
	 * @author bms
	 */
	public class OffenceOverlay extends fLayer
	{
		public var creepButtons:fObjectPool;
		
		public var background:Quad;
		
		public var buttonsLayer:Sprite;
		
		public var addCreepButton:fButton;
		
		private var oldx:Number;
		
		private var dragging:Boolean = false;
		private var clickx:Number;
		
		public function OffenceOverlay()
		{
			background = new Quad(fGame.width, GameState.GRID_SIZE_IN_PIXELS * 2, 0x302827);
			background.pivotX = background.width / 2;
			background.pivotY = background.height / 2;
			background.x = fGame.width / 2;
			background.y = fGame.height - (fGame.height - 16 * GameState.GRID_SIZE_IN_PIXELS) / 2 - GameState.GRID_SIZE_IN_PIXELS/2;
			addChild(background);
			
			buttonsLayer = new Sprite();
			addChild(buttonsLayer);
			
			creepButtons = new fObjectPool();
			for (var i:int = 0; i < 100; i++)
			{
				var cb:CreepButton = new CreepButton();
				cb.visible = false;
				creepButtons.addObject(cb);
				cb.addEventListener(Event.TRIGGERED, onTouchListener);
				buttonsLayer.addChild(cb);
			}
			
			addCreepButton = new fButton(fAssetManager.getTexture("game", "addCreepButton"));
			addCreepButton.visible = false;
			addCreepButton.pivotX = addCreepButton.width / 2;
			addCreepButton.pivotY = addCreepButton.height / 2;
			addCreepButton.addEventListener(Event.TRIGGERED, onAddCreepTouched);
			buttonsLayer.addChild(addCreepButton);
			
			addEventListener(TouchEvent.TOUCH, onScrollListerner);
		}
		
		private function onScrollListerner(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this as DisplayObject);
			const threshold:Number = 20;
			
			if (touch && TouchPhase.MOVED == touch.phase)
			{
				if (Math.abs(touch.globalX - clickx) > threshold && buttonsLayer.width > background.width)
				{
					dragging = true;
				}
				if (dragging)
				{
					var dx:Number = touch.globalX - oldx;
					buttonsLayer.x += dx;
					if (buttonsLayer.x > 0)
					{
						buttonsLayer.x = 0;
					} else if (buttonsLayer.x < -buttonsLayer.width + background.width)
					{
						buttonsLayer.x = -buttonsLayer.width + background.width;
					}
				}
				oldx = touch.globalX;
			} else if (touch && TouchPhase.ENDED == touch.phase)
			{
				dragging = false;
			} else if (touch && TouchPhase.HOVER == touch.phase)
			{
				oldx = touch.globalX;
			} else if (touch && TouchPhase.BEGAN == touch.phase)
			{
				clickx = touch.globalX;
			}
		}
		
		public function displayWave():void
		{
			var i:int;
			
			for each(var el:fIPoolable in creepButtons.objects)
				el.kill();
			
			var state:GameState = fGame.state as GameState;
			
			for (i = 0; i < state.inactiveCreeps.length; i++)
			{
				var cb:CreepButton = creepButtons.getObject() as CreepButton;
				cb.text = (i+1).toString();
				cb.id = state.inactiveCreeps[i].id;
				cb.x = GameState.GRID_SIZE_IN_PIXELS / 2 + (i + 0.5) * (cb.width + 2);
				cb.y = background.y;
			}
			addCreepButton.visible = true;
			addCreepButton.x = GameState.GRID_SIZE_IN_PIXELS / 2 + (i + 0.5) * (addCreepButton.width + 2);
			addCreepButton.y = background.y;
		}
		
		private function onAddCreepTouched(e:Event):void
		{
			if (!dragging)
			{
				SocketManager.reqNewCreep();
			}
		}
		
		private function onTouchListener(e:Event):void
		{
			if (!dragging)
			{
				var state:GameState = fGame.state as GameState;
				state.upgradeCreepOverlay.visible = true;
				var id:String = state.upgradeCreepOverlay.creepID = (e.target as CreepButton).id;
				state.upgradeCreepOverlay.creepSpeed = state.getInactiveCreepByID(id).speed;
				state.upgradeCreepOverlay.creepDamage = state.getInactiveCreepByID(id).damage;
				state.upgradeCreepOverlay.creepHeath = state.getInactiveCreepByID(id).health;
			}
		}
	}
}