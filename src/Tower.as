package  
{
	import com.mikesoylu.fortia.*;
	import starling.core.Starling;
	import starling.events.Event;
	
	/**
	 * @author bms
	 */
	public class Tower extends fImage implements fIPoolable
	{
		public static const ATTACK_CLOSE:String = "attackClose";
		public static const ATTACK_WEAK:String = "attackWeak";
		public static const ATTACK_STRONG:String = "attackStrong";
		
		public var team:String;
		
		public var id:String;
		
		public var damage:Number;
		
		private var _target:Creep;
		
		public var speed:Number = 1;
		
		public var rangeSqr:Number = 16*16*2*2;
		
		public var attackStyle:String;
		
		public var active:Boolean;
		
		public function Tower()
		{
			super(fAssetManager.getTexture("game", "tower"));
			pivotX = width / 2;
			pivotY = height / 2;
			visible = false;
		}
		
		public function revive():void 
		{
			visible = true;
		}
		
		public function get alive():Boolean 
		{
			return visible;
		}
		
		public function set target(value:Creep):void 
		{
			_target = value;
		}
		
		public function kill():void 
		{
			visible = false;
		}
		
		public function attack():void
		{
			if (active && visible && _target && _target.visible)
			{
				var dx = (_target.x - x);
				var dy = (_target.y - y);
				if (dx * dx + dy * dy < rangeSqr)
				{
					(fGame.state as GameState).fire(this, _target);
				} else
				{
					_target = null;
				}
			}
			if (visible && active)
			{
				Starling.juggler.delayCall(attack, speed*0.001);
			}
		}
	}
}