package  
{
	import com.mikesoylu.fortia.*;
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.*;
	import starling.textures.*;
	import flash.geom.Point;
	
	/**
	 * @author bms
	 */
	public class Creep extends fMovieClip implements fIPoolable
	{
		
		public var path:Array;
		
		public var team:String;
		public var health:Number;
		public var damage:Number;
		public var speed:Number;
		public var currentCell:int;
		
		public function Creep(path:Array) 
		{
			super(fAssetManager.getAtlas("game").getTextures("creep"));
			
			this.path = path;
			
			pivotX = width / 2;
			pivotY = height / 2;
			Starling.juggler.add(this);
			
			visible = false;
		}
		
		public function revive():void 
		{
			visible = true;
			x = -2000;
			y = -2000;
		}
		
		public function get alive():Boolean 
		{
			return visible;
		}
		
		public function kill():void 
		{
			visible = false;
		}
		
		public function advanceCell():void 
		{
			var i:int = fUtil.clamp(0, path.length - 1, currentCell);
			var cc = path[i];
			if (i >= 0 && i <= path.length-1)
			{
				//var tween:Tween = new Tween(this, speed, Transitions.LINEAR);
				//tween.moveTo(GameState.GRID_SIZE_IN_PIXELS/2+cc.x*GameState.GRID_SIZE_IN_PIXELS, GameState.GRID_SIZE_IN_PIXELS/2+cc.y*GameState.GRID_SIZE_IN_PIXELS);
				//Starling.juggler.add(tween);
				x = GameState.GRID_SIZE_IN_PIXELS / 2 + cc.x * GameState.GRID_SIZE_IN_PIXELS;
				y = GameState.GRID_SIZE_IN_PIXELS / 2 + cc.y * GameState.GRID_SIZE_IN_PIXELS;
			}
			
			if (team == GameState.TEAM_A)
			{
				currentCell--;
				
			} else if (team == GameState.TEAM_B)
			{
				currentCell++;
			}
			
			if (team == GameState.TEAM_B && currentCell<path.length)
			{
				Starling.juggler.delayCall(advanceCell, speed*0.001);
				
			}else if (team == GameState.TEAM_A && currentCell>=0)
			{
				Starling.juggler.delayCall(advanceCell, speed*0.001);
			}else 
			{
				kill();
			}
		}
	}
}