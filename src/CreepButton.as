package  
{
	import com.mikesoylu.fortia.fAssetManager;
	import com.mikesoylu.fortia.fButton;
	import com.mikesoylu.fortia.fIPoolable;
	import flash.display.Bitmap;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	/**
	 * @author bms
	 */
	public class CreepButton extends fButton implements fIPoolable
	{
		public var id:String;
		
		public function CreepButton() 
		{
			super(fAssetManager.getTexture("game", "modCreepButton"), " ");
			
			mTextField.fontName = "Visitor TT1 BRK";
			mTextField.fontSize = BitmapFont.NATIVE_SIZE;
			mTextField.color = Color.WHITE;
			
			pivotX = width / 2;
			pivotY = height / 2;
		}
		
		public function revive():void 
		{
			visible = true;
			touchable = true;
		}
		
		public function get alive():Boolean 
		{
			return visible;
		}
		
		public function kill():void 
		{
			visible = false;
			touchable = false;
		}
	}
}