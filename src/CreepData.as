package  
{
	/**
	 * @author bms
	 */
	public class CreepData 
	{
		public var health:Number;
		public var speed:Number;
		public var damage:Number;
		public var initialIndex:int;
		public var id:String;
		
		public function CreepData(health:Number, speed:Number, damage:Number, initialIndex:int, id:String) 
		{
			this.health = health;
			this.speed = speed;
			this.damage = damage;
			this.initialIndex = initialIndex;
			this.id = id;
		}
	}
}