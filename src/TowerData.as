package  
{
	import flash.geom.Point;
	/**
	 * @author bms
	 */
	public class TowerData 
	{
		public var range:Number;
		public var speed:Number;
		public var damage:Number;
		public var position:Point;
		public var id:String;
		public var team:String;
		
		public function TowerData(range:Number, speed:Number, damage:Number, position:Point, id:String, team:String) 
		{
			this.range = range;
			this.speed = speed;
			this.damage = damage;
			this.position = position;
			this.id = id;
			this.team = team;
		}
		public function toString():String
		{
			var s:String = range + "," + speed + "," + damage + "," +position + "," + id + "," + team;
			return s;
		}
	}
}