package  
{
	import com.mikesoylu.fortia.fAssetManager;
	import com.mikesoylu.fortia.fButton;
	import com.mikesoylu.fortia.fSprite;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.VAlign;
	
	/**
	 * @author bms
	 */
	public class MessageOverlay extends fSprite 
	{
		private var message:TextField;
		
		public function MessageOverlay() 
		{
			var background:Image = new Image(fAssetManager.getTexture("game", "message"));
			background.pivotX = background.width * 0.5;
			background.pivotY = background.height * 0.5;
			addChild(background);
			
			message = new TextField(background.width - 4, background.height-4, "Message:\n\nasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf");
			message.pivotX = message.width / 2;
			message.pivotY = message.height / 2;
			message.vAlign = VAlign.TOP;
			message.fontName = "Visitor TT1 BRK";
			message.fontSize = BitmapFont.NATIVE_SIZE;
			message.color = Color.WHITE;
			message.y = 2;
			addChild(message);
			
			var okButton:fButton = new fButton(fAssetManager.getTexture("game", "okButton"),"ok");
			okButton.pivotX = okButton.width / 2;
			okButton.pivotY = okButton.height + 2;
			okButton.y = background.height / 2;
			okButton.fontName = "Visitor TT1 BRK";
			okButton.fontSize = BitmapFont.NATIVE_SIZE;
			okButton.fontColor = Color.WHITE;
			okButton.addEventListener(Event.TRIGGERED, onOkListener);
			addChild(okButton);
			
			visible = false;
		}
		
		private function onOkListener(e:Event):void 
		{
			visible = false;
		}
		public function showMessage(str:String):void
		{
			message.text = "Message:\n\n" + str;
			visible = true;
		}
	}
}