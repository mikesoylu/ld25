package  
{
	import com.mikesoylu.fortia.fAssetManager;
	import com.mikesoylu.fortia.fImage;
	
	/**
	 * ...
	 * @author bms
	 */
	public class PathCell extends fImage 
	{
		
		public function PathCell(x:Number, y:Number) 
		{
			super(fAssetManager.getTexture("game", "cell"));
			pivotX = width / 2;
			pivotY = height / 2;
			
			this.x = x;
			this.y = y;
		}
	}
}