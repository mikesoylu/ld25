package  
{
	import com.mikesoylu.fortia.fAssetManager;
	import starling.display.Image;
	/**
	 * ...
	 * @author bms
	 */
	public class GridCell extends Image
	{
		public var isInRange:Boolean = false;
		
		public function GridCell() 
		{
			super(fAssetManager.getTexture("game", "grid"));
			pivotX = width / 2;
			pivotY = height / 2;
		}
		
	}

}