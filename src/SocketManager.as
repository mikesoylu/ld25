package
{
	import com.mikesoylu.fortia.fGame;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Point;
	import io.*;
	
	/**
	 * @author bms
	 */
	public class SocketManager
	{
		public var socket:SocketNamespace;
		
		private static var _instance:SocketManager;
		
		public function SocketManager():void
		{
			var options:Options = new Options();
			try
			{
				socket = IO.connect('192.168.1.4:31168', options);
			} catch (e:IOError)
			{
				trace(e);
			}
		}
		
		public static function reqNewTower(x:Number, y:Number):void
		{
			instance.socket.emit('add tower', {x:x, y:y});
			trace("reqNewTower(" + x + ", " + y + ")");
		}
		
		public static function reqNewCreep():void
		{
			instance.socket.emit('add creep');
			trace("reqNewCreep()");
		}
		
		static public function reqPlayerState(username:String):void
		{
			GameState.playerName = username;
			instance.socket.emit("nick", username );
			instance.socket.on("initial data", onInitialDataCallback);
			instance.socket.on("added tower", onAddedTowerCallback);
			instance.socket.on("added creep", onAddedCreepCallback);
			instance.socket.on("upgraded tower", onUpgradedTowerCallback);
			instance.socket.on("upgraded creep", onUpgradedCreepCallback);
			instance.socket.on("message", onMessageCallback);
			instance.socket.on("new round", onNewRound);
			instance.socket.on("game finished", onGameFinished);
			trace("reqPlayerState(",username,")");
		}
		
		static public function addSpeedToTower(towerID:String):void 
		{
			trace("addSpeedToTower(", towerID, ")");
			instance.socket.emit("upgrade tower", { "id":towerID, "stat":"speed" } );
		}
		
		static public function addDamageToTower(towerID:String):void 
		{
			trace("addDamageToTower(", towerID, ")");
			instance.socket.emit("upgrade tower", { "id":towerID, "stat":"damage" } );
		}
		
		static public function addRangeToTower(towerID:String):void 
		{
			trace("addRangeToTower(", towerID, ")");
			instance.socket.emit("upgrade tower", { "id":towerID, "stat":"range" } );
		}
		
		// 
		static public function addSpeedToCreep(creepID:String):void 
		{
			trace("addSpeedToCreep(", creepID, ")");
			instance.socket.emit("upgrade creep", { "id":creepID, "stat":"speed" } );
		}
		
		static public function addHealthToCreep(creepID:String):void 
		{
			trace("addHealthToCreep(", creepID, ")");
			instance.socket.emit("upgrade creep", { "id":creepID, "stat":"health" } );
		}
		
		static public function addDamageToCreep(creepID:String):void 
		{
			trace("addDamageToCreep(", creepID, ")");
			instance.socket.emit("upgrade creep", { "id":creepID, "stat":"damage" } );
		}
		
		static public function get instance():SocketManager 
		{
			if (null == _instance)
			{
				_instance = new SocketManager();
			}
			return _instance;
		}
		
		/////////// callbacks ////////
		
		static private function onInitialDataCallback(data:Object):void 
		{
			// listen once
			if (fGame.state is LoginState)
			{
				GameState.path = data.map as Array;
				GameState.playerTeam = data.team.name as String;
				GameState.playerCredits = data.gold;
				if (data.mode == "offense")
				{
					GameState.offenseTeam = data.team.name as String;
				} else
				{
					if (GameState.playerTeam == GameState.TEAM_A)
						GameState.offenseTeam = GameState.TEAM_B;
					else
						GameState.offenseTeam = GameState.TEAM_A;
				}
				
				var towers:Vector.<TowerData> = new Vector.<TowerData>();
				
				fGame.state = new GameState();
				
				if (data.turn.towers)
				{
					for each(var twr:Object in data.turn.towers as Array)
					{
						towers.push(new TowerData(twr.range, twr.speed, twr.damage, new Point(twr.x, twr.y), twr.id, twr.team));
					}
				}
				
				(fGame.state as GameState).drawActiveTowers(towers);
				
				if (data.team.creeps)
				{
					for each(var cro:Object in data.team.creeps as Array)
					{
						(fGame.state as GameState).drawInactiveCreep(new CreepData(cro.health, cro.speed, cro.damage, 0, cro.id));
					}
				}
				
				if (data.team.towers)
				{
					
					for each(var twr:Object in data.team.towers as Array)
					{
						(fGame.state as GameState).drawInactiveTower(new TowerData(twr.range, twr.speed, twr.damage, new Point(twr.x, twr.y), twr.id, GameState.playerTeam));
					}
				}
			}
		}
		
		static private function onNewRound(data:Object):void 
		{
			if (fGame.state is GameState)
			{
				if (data.mode == "offense")
				{
					GameState.offenseTeam = GameState.playerTeam;
				} else
				{
					if (GameState.playerTeam == GameState.TEAM_A)
						GameState.offenseTeam = GameState.TEAM_B;
					else
						GameState.offenseTeam = GameState.TEAM_A;
				}
				
				GameState.playerCredits = data.gold;
				
				var creeps:Vector.<CreepData> = new Vector.<CreepData>();
				var towers:Vector.<TowerData> = new Vector.<TowerData>();
				
				fGame.state = new GameState();
				
				if (data.turn.creeps)
				{
					for each(var cro:Object in data.turn.creeps as Array)
					{
						creeps.push(new CreepData(cro.health, cro.speed, cro.damage, cro.mapIndex, cro.id));
					}
				}
				
				if (data.turn.towers)
				{
					for each(var twr:Object in data.turn.towers as Array)
					{
						towers.push(new TowerData(twr.range, twr.speed, twr.damage, new Point(twr.x, twr.y), twr.id, twr.team));
					}
				}
				
				(fGame.state as GameState).drawActiveCreeps(creeps);
				(fGame.state as GameState).drawActiveTowers(towers);
				
				if (data.team.creeps)
				{
					for each(var cro:Object in data.team.creeps as Array)
					{
						(fGame.state as GameState).drawInactiveCreep(new CreepData(cro.health, cro.speed, cro.damage, 0, cro.id));
					}
				}
				
				if (data.team.towers)
				{
					
					for each(var twr:Object in data.team.towers as Array)
					{
						(fGame.state as GameState).drawInactiveTower(new TowerData(twr.range, twr.speed, twr.damage, new Point(twr.x, twr.y), twr.id, GameState.playerTeam));
					}
				}
			}
		}
		
		static private function onUpgradedCreepCallback(data:Object):void 
		{
			var crp:CreepData = (fGame.state as GameState).getInactiveCreepByID(data.creep.id);
			crp.health = data.creep.health;
			crp.speed = data.creep.speed;
			crp.damage = data.creep.damage;
			
			(fGame.state as GameState).upgradeCreepOverlay.creepSpeed = crp.speed;
			(fGame.state as GameState).upgradeCreepOverlay.creepDamage= crp.damage;
			(fGame.state as GameState).upgradeCreepOverlay.creepHeath = crp.health;
			
			if (data.player == GameState.playerName)
				GameState.playerCredits--;
		}
		
		static private function onUpgradedTowerCallback(data:Object):void 
		{
			var twr:Tower = (fGame.state as GameState).getInactiveTowerByID(data.tower.id);
			twr.rangeSqr = data.tower.range * data.tower.range;
			twr.speed = data.tower.speed;
			twr.damage = data.tower.damage;
			
			(fGame.state as GameState).upgradeTowerOverlay.towerSpeed = twr.speed;
			(fGame.state as GameState).upgradeTowerOverlay.towerDamage= twr.damage;
			(fGame.state as GameState).upgradeTowerOverlay.towerRange = data.tower.range;
			if (data.player == GameState.playerName)
				GameState.playerCredits--;
		}
		
		static private function onGameFinished(data:Object):void 
		{
			if (GameState.playerTeam == data.loser)
			{
				var msg:String = "your team lost the game.";
			} else
			{
				msg = "your team won!"
			}
			(fGame.state as GameState).messageOverlay.showMessage(data as String);
		}
		
		static private function onAddedCreepCallback(data:Object):void 
		{
			var crd = data.creep;
			(fGame.state as GameState).drawInactiveCreep(new CreepData(crd.health, crd.speed, crd.damage, 0, crd.id));
			if (data.player == GameState.playerName)
				GameState.playerCredits--;
		}
		
		static private function onAddedTowerCallback(data:Object):void 
		{
			var twd = data.tower;
			(fGame.state as GameState).drawInactiveTower(new TowerData(twd.range, twd.speed, twd.damage, new Point(twd.x, twd.y), twd.id, GameState.playerTeam));
			if (data.player == GameState.playerName)
				GameState.playerCredits--;
		}
		
		static private function onMessageCallback(data:Object):void 
		{
			(fGame.state as GameState).messageOverlay.showMessage(data as String);
		}
	}
}