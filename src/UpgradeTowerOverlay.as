package  
{
	import com.mikesoylu.fortia.fAssetManager;
	import com.mikesoylu.fortia.fButton;
	import com.mikesoylu.fortia.fGame;
	import com.mikesoylu.fortia.fSprite;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.utils.Color;
	
	/**
	 * @author bms
	 */
	public class UpgradeTowerOverlay extends fSprite 
	{
		private var background:Quad;
		
		public var towerID:String;

		private var speedButton:fButton;
		private var damageButton:fButton;
		private var rangeButton:fButton;
		private var okButton:fButton;
		
		public function UpgradeTowerOverlay() 
		{
			background = new Quad(fGame.width, GameState.GRID_SIZE_IN_PIXELS*5, 0x302827);
			background.pivotX = background.width / 2;
			background.pivotY = background.height / 2;
			background.x = fGame.width / 2;
			background.y = fGame.height / 2;
			addChild(background);
			
			speedButton = new fButton(fAssetManager.getTexture("game", "button"), "delay - 1234");
			speedButton.fontName = "Visitor TT1 BRK";
			speedButton.fontSize = BitmapFont.NATIVE_SIZE;
			speedButton.fontColor = Color.WHITE;
			speedButton.pivotX = speedButton.width / 2;
			speedButton.pivotY = speedButton.height / 2;
			speedButton.x = fGame.width / 2;
			speedButton.y = fGame.height / 2 - 1.5*GameState.GRID_SIZE_IN_PIXELS- 3;
			speedButton.addEventListener(Event.TRIGGERED, onSpeedButton);
			addChild(speedButton);
			
			damageButton = new fButton(fAssetManager.getTexture("game", "button"), "damage - 1234");
			damageButton.fontName = "Visitor TT1 BRK";
			damageButton.fontSize = BitmapFont.NATIVE_SIZE;
			damageButton.fontColor = Color.WHITE;
			damageButton.pivotX = damageButton.width / 2;
			damageButton.pivotY = damageButton.height / 2;
			damageButton.x = fGame.width / 2;
			damageButton.y = fGame.height / 2 - 0.5*GameState.GRID_SIZE_IN_PIXELS - 1;
			damageButton.addEventListener(Event.TRIGGERED, onDamageButton);
			addChild(damageButton);
			
			rangeButton = new fButton(fAssetManager.getTexture("game", "button"), "range - 1234");
			rangeButton.fontName = "Visitor TT1 BRK";
			rangeButton.fontSize = BitmapFont.NATIVE_SIZE;
			rangeButton.fontColor = Color.WHITE;
			rangeButton.pivotX = rangeButton.width / 2;
			rangeButton.pivotY = rangeButton.height / 2;
			rangeButton.x = fGame.width / 2;
			rangeButton.y = fGame.height / 2 + 0.5*GameState.GRID_SIZE_IN_PIXELS + 1;
			rangeButton.addEventListener(Event.TRIGGERED, onRangeButton);
			addChild(rangeButton);
			
			okButton = new fButton(fAssetManager.getTexture("game", "okButton"), "ok");
			okButton.fontName = "Visitor TT1 BRK";
			okButton.fontSize = BitmapFont.NATIVE_SIZE;
			okButton.fontColor = Color.WHITE;
			okButton.pivotX = okButton.width / 2;
			okButton.pivotY = okButton.height / 2;
			okButton.x = fGame.width / 2;
			okButton.y = fGame.height / 2 + 1.5*GameState.GRID_SIZE_IN_PIXELS + 3;
			okButton.addEventListener(Event.TRIGGERED, onOkButton);
			addChild(okButton);
		}
		
		public function set towerSpeed(n:Number):void
		{
			speedButton.text = "delay - " + n;
		}
		public function set towerDamage(n:Number):void
		{
			damageButton.text = "damage - " + n;
		}
		public function set towerRange(n:Number):void
		{
			rangeButton.text = "range - " + n;
		}
		
		private function onSpeedButton(e:Event):void
		{
			SocketManager.addSpeedToTower(towerID);
		}
		private function onDamageButton(e:Event):void 
		{
			SocketManager.addDamageToTower(towerID);
		}
		
		private function onRangeButton(e:Event):void 
		{
			SocketManager.addRangeToTower(towerID);
		}
		
		private function onOkButton(e:Event):void 
		{
			this.visible = false;
		}
	}
}