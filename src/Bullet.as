package  
{
	import com.mikesoylu.fortia.*;
	import starling.animation.Tween;
	import starling.core.Starling;
	
	/**
	 * @author bms
	 */
	public class Bullet extends fImage implements fIPoolable
	{
		
		public function Bullet() 
		{
			super(fAssetManager.getTexture("game", "beam"));
			pivotX = 0;
			pivotY = height / 2;
			visible = false;
		}
		
		public function revive():void 
		{
			visible = true;
			alpha = 1;
		}
		
		public function beamTo(target:Creep):void
		{
			var dx = target.x - x;
			var dy = target.y - y;
			var dd = Math.sqrt(dx * dx + dy * dy);
			
			scaleX = dd;
			rotation = Math.atan2(dy, dx);
			
			var tween:Tween = new Tween(this, 0.5);
			tween.fadeTo(0);
			
			Starling.juggler.delayCall(kill, 0.5);
			Starling.juggler.add(tween);
		}
		
		override public function update(dt:Number):void 
		{
			super.update(dt);
			
			scaleY = Math.random() + 0.5;
		}
		
		public function get alive():Boolean 
		{
			return visible;
		}
		
		public function kill():void 
		{
			visible = false;
		}
	}
}