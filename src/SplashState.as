package  
{
	import com.mikesoylu.fortia.fAssetManager;
	import com.mikesoylu.fortia.fButton;
	import com.mikesoylu.fortia.fGame;
	import com.mikesoylu.fortia.fState;
	import flash.display.Bitmap;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.VAlign;
	
	/**
	 * ...
	 * @author bms
	 */
	public class SplashState extends fState 
	{
		public var story:String = "It is a period of civil war.\n\nThe lesser evils have rebelled against the sinister Kittyblo and his minions.\n\nStriking each other with Big Foul Goats, the two sides fight to become the true masters of the universe."
		
		override public function init(e:Event):void 
		{
			super.init(e);
			
			var message:TextField;
			fAssetManager.addTextureAtlas("game", Assets.SPRITE_SHEET_PNG, Assets.SPRITE_SHEET_XML);
			
			var bitmap:Bitmap = new Assets.VISITOR_PNG();
			var texture:Texture = Texture.fromBitmap(bitmap);
			var xml:XML = XML(new Assets.VISITOR_FNT());
			TextField.registerBitmapFont(new BitmapFont(texture, xml));
			
			
			var background:Quad = new Quad(fGame.width,fGame.height,0x3C3736);
			addChild(background);
			
			message = new TextField(background.width /3, background.height/2, story);
			message.pivotX = message.width / 2;
			message.pivotY = message.height / 2;
			message.vAlign = VAlign.TOP;
			message.fontName = "Visitor TT1 BRK";
			message.fontSize = BitmapFont.NATIVE_SIZE;
			message.color = Color.WHITE;
			message.y = background.height/2;
			message.x = background.width /2;
			addChild(message);
			
			var okButton:fButton = new fButton(fAssetManager.getTexture("game", "okButton"),"begin");
			okButton.pivotX = okButton.width / 2;
			okButton.pivotY = okButton.height + 2;
			okButton.y = message.y+message.height / 2;
			okButton.x = background.width /2;
			okButton.fontName = "Visitor TT1 BRK";
			okButton.fontSize = BitmapFont.NATIVE_SIZE;
			okButton.fontColor = Color.WHITE;
			okButton.addEventListener(Event.TRIGGERED, onOkListener);
			addChild(okButton);
			
		}
		
		private function onOkListener(e:Event):void 
		{
			fGame.state = new LoginState();
		}
	}

}