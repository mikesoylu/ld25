package  
{
	import com.mikesoylu.fortia.*;
	
	/**
	 * @author bms
	 */
	public class OffenceMode implements fMode 
	{
		private var parent:GameState;
		
		public function init(parent:fState):void 
		{
			this.parent = parent as GameState;
		}
		
		public function update(dt:Number):void 
		{
			
		}
		
		public function activate():void 
		{
			parent.offenceOverlay.visible = true;
			if (GameState.playerTeam == GameState.TEAM_B)
			{
				parent.messageOverlay.showMessage("prepare your attacking wave for next round\n you will attack from the right");
			} else
			{
				parent.messageOverlay.showMessage("prepare your attacking wave for next round\n you will attack from the left");
			}
		}
		
		public function deactivate():void 
		{
			parent.offenceOverlay.visible = false;
			parent.upgradeCreepOverlay.visible = false;
		}
		
	}
}