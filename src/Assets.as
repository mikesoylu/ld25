package  
{
	/**
	 * @author bms
	 */
	public class Assets 
	{
		[Embed(source="../assets/fnt/visitor_0.png")]
		public static const VISITOR_PNG:Class;
		[Embed(source="../assets/fnt/visitor.fnt", mimeType="application/octet-stream")]
		public static const VISITOR_FNT:Class;
		
		[Embed(source = "../assets/img/SpriteSheet.png")]
		public static const SPRITE_SHEET_PNG:Class;
		[Embed(source = "../assets/img/SpriteSheet.xml", mimeType="application/octet-stream")]
		public static const SPRITE_SHEET_XML:Class;
	}
}