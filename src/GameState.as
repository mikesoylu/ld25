package  
{
	import adobe.utils.CustomActions;
	import com.mikesoylu.fortia.*;
	import starling.core.Starling;
	import starling.display.*;
	import starling.events.*;
	import starling.text.*;
	import starling.textures.*;
	import starling.utils.*;

	import flash.display.Bitmap;
	import flash.geom.Point;

	/**
	 * @author bms
	 */
	public class GameState extends fState 
	{
		public static const TEAM_A:String = "goats";
		public static const TEAM_B:String = "wolves";
		
		public static var playerTeam:String = TEAM_B;
		public static var offenseTeam:String = TEAM_B;
		
		public static const GRID_SIZE_IN_PIXELS:int = 16;
		
		public var pathCells:Vector.<PathCell>;
		
		public static var path:Array;
		static public var playerCredits:int;
		static public var playerName:String;
		
		public var inactiveCreeps:Vector.<CreepData>;
		
		// 
		public var creeps:fObjectPool;
		public var bullets:fObjectPool;
		public var towers:fObjectPool;
		
		public var currentMode:fMode;
		public var defenceMode:DefenceMode;
		public var offenceMode:OffenceMode;
		
		public var offenceOverlay:OffenceOverlay;
		public var upgradeCreepOverlay:UpgradeCreepOverlay;
		public var upgradeTowerOverlay:UpgradeTowerOverlay;
		
		//
		private var upperLayer:fSprite;
		private var gridLayer:fSprite;
		private var gridCells:Vector.<GridCell>;

		private var background:Quad;
		private var creditsLabel:fButton;
		
		public var messageOverlay:MessageOverlay;
		
		override public function init(e:Event):void 
		{
			super.init(e);
			
			background = new Quad(fGame.width, fGame.height, 0x261918);
			addChild(background);
			
			gridLayer = new fSprite();
			gridCells = new Vector.<GridCell>();
			for (var j:int = 0; j < 28 * 16; j++)
			{
				var qu:GridCell = new GridCell();
				qu.x = GRID_SIZE_IN_PIXELS / 2 + j % 28 * GRID_SIZE_IN_PIXELS;
				qu.y = GRID_SIZE_IN_PIXELS / 2 + int(j / 28) * GRID_SIZE_IN_PIXELS;
				gridCells.push(qu);
				gridLayer.addChild(qu);
			}
			gridLayer.x = (fGame.width - gridLayer.width) / 2;
			addChild(gridLayer);
			
			pathCells = new Vector.<PathCell>();
			
			upperLayer = new fSprite();
			upperLayer.x = (fGame.width - gridLayer.width) / 2;
			addChild(upperLayer);
			
			creeps = new fObjectPool();
			bullets = new fObjectPool();
			towers = new fObjectPool();
			
			inactiveCreeps = new Vector.<CreepData>();
			
			var i:int;
			for (i = 0; i < 200; i++)
			{
				upperLayer.addChild(creeps.addObject(new Creep(path)) as DisplayObject);
			}
			
			for (i = 0; i < 200; i++)
			{
				upperLayer.addChild(towers.addObject(new Tower()) as DisplayObject);
			}
			
			for (i = 0; i < 200; i++)
			{
				upperLayer.addChild(bullets.addObject(new Bullet()) as DisplayObject);
			}
			
			drawPath();
			gridLayer.flatten();
			
			offenceOverlay = new OffenceOverlay();
			offenceOverlay.visible = false;
			addChild(offenceOverlay);
			
			upgradeCreepOverlay = new UpgradeCreepOverlay();
			upgradeCreepOverlay.visible = false;
			upgradeTowerOverlay = new UpgradeTowerOverlay();
			upgradeTowerOverlay.visible = false;
			
			addChild(upgradeCreepOverlay);
			addChild(upgradeTowerOverlay);
			
			messageOverlay = new MessageOverlay();
			messageOverlay.x = fGame.width * 0.5;
			messageOverlay.y = fGame.height * 0.5;
			addChild(messageOverlay);

			creditsLabel = new fButton(fAssetManager.getTexture("game", "credits"), "gold - " + playerCredits);
			creditsLabel.fontName = "Visitor TT1 BRK";
			creditsLabel.fontSize = BitmapFont.NATIVE_SIZE;
			creditsLabel.fontColor = Color.WHITE;
			creditsLabel.pivotX = creditsLabel.width * 0.5;
			creditsLabel.pivotY = creditsLabel.height;
			creditsLabel.addEventListener(Event.TRIGGERED, onCreditsLabelTouched);
			creditsLabel.x = fGame.width * 0.5;
			creditsLabel.y = fGame.height;
			addChild(creditsLabel);
			
			offenceMode = new OffenceMode();
			offenceMode.init(this);
			defenceMode = new DefenceMode();
			defenceMode.init(this);
			
			if (playerTeam == offenseTeam)
			{
				currentMode = offenceMode;
				offenceOverlay.displayWave();
			} else 
			{
				currentMode = defenceMode;
			}
			currentMode.activate();
		}
		
		private function onCreditsLabelTouched():void 
		{
			messageOverlay.showMessage("you can buy & upgrade units with creds");
		}
		
		// temp function before server stuff
		public function drawPath():void 
		{
			for (var i:int = 0; i < path.length; i++)
			{
				var startX = GRID_SIZE_IN_PIXELS/2 + path[i].x * GRID_SIZE_IN_PIXELS;
				var startY = GRID_SIZE_IN_PIXELS/2 + path[i].y * GRID_SIZE_IN_PIXELS;
				
				var c = new PathCell(startX, startY);
				pathCells.push(c);
				gridLayer.addChild(c);
			}
		}
		
		public function drawActiveCreeps(activeCreeps:Vector.<CreepData>):void
		{
			for (var i:int = 0; i < activeCreeps.length; i++)
			{
				var cr:Creep = creeps.getObject() as Creep;
				var actcr:CreepData = activeCreeps[i];
				cr.team = offenseTeam;
				cr.currentCell = actcr.initialIndex;
				cr.speed = actcr.speed;
				cr.health = actcr.health;
				cr.damage = actcr.damage;
				
				cr.advanceCell();
			}
		}
		
		public function drawActiveTowers(activeTowers:Vector.<TowerData>):void
		{
			for (var i:int = 0; i < activeTowers.length; i++)
			{
				var t:Tower = towers.getObject() as Tower;
				var actt:TowerData = activeTowers[i];
				
				t.id = actt.id;
				t.team = actt.team;
				t.speed = actt.speed;
				t.rangeSqr = actt.range * actt.range * GRID_SIZE_IN_PIXELS * GRID_SIZE_IN_PIXELS;
				t.damage = actt.damage;
				t.x = (actt.position.x + 0.5) * GRID_SIZE_IN_PIXELS;
				t.y = (actt.position.y + 0.5) * GRID_SIZE_IN_PIXELS;
				t.active = true;
				t.attack();
				var dx;
				var dy;
				if (actt.team == playerTeam)
				{
					for each (var gr:GridCell in gridCells)
					{
						dx = gr.x - t.x;
						dy = gr.y - t.y;
						if (dx * dx + dy * dy <= t.rangeSqr)
						{
							gr.color = 0xAAAAAA;
							gr.isInRange = true;
						}
					}
				}
			}
		}
		
		public function drawInactiveTower(actt:TowerData):void
		{
			var t:Tower = towers.getObject() as Tower;
			trace("drawing tower: "+actt);
			t.id = actt.id;
			t.team = playerTeam;
			t.speed = actt.speed;
			t.rangeSqr = actt.range * actt.range * GRID_SIZE_IN_PIXELS * GRID_SIZE_IN_PIXELS;
			t.damage = actt.damage;
			t.x = (actt.position.x + 0.5) * GRID_SIZE_IN_PIXELS;
			t.y = (actt.position.y + 0.5) * GRID_SIZE_IN_PIXELS;
			t.active = false;
			var dx;
			var dy;
			for each (var gr:GridCell in gridCells)
			{
				dx = gr.x - t.x;
				dy = gr.y - t.y;
				if (dx * dx + dy * dy <= t.rangeSqr)
				{
					gr.color = 0xAAAAAA;
					gr.isInRange = true;
				}
			}
		}
		
		public function drawInactiveCreep(actcr:CreepData):void
		{
			inactiveCreeps.push(actcr);
			offenceOverlay.displayWave();
		}
		
		public function getInactiveCreepByID(id:String):CreepData
		{
			for (var i:int = 0; i < inactiveCreeps.length; i++)
				if (id == inactiveCreeps[i].id)
				{
					return inactiveCreeps[i];
				}
			return null;
		}
		public function getInactiveTowerByID(id:String):Tower 
		{
			for (var i:int = 0; i < towers.objects.length; i++)
				if (!(towers.objects[i] as Tower).active && id == (towers.objects[i] as Tower).id)
				{
					return towers.objects[i] as Tower;
				}
			return null;
		}
		
		override public function update(dt:Number):void 
		{
			super.update(dt);
			
			creditsLabel.text = "gold - " + playerCredits;
			
			for (var i:int = 0; i < towers.objects.length; i++)
			{
				var tw:Tower = towers.objects[i] as Tower;
				if (tw.alive && tw.active && tw.team != offenseTeam)
				{
					for (var j:int = 0; j < creeps.objects.length; j++)
					{
						var cr:Creep = creeps.objects[j] as Creep;
						var dx = cr.x - tw.x;
						var dy = cr.y - tw.y;
						var dd = dx * dx + dy * dy;
						
						if (cr.alive && dd<= tw.rangeSqr)
						{
							tw.target = cr;
						}
					}
				}
			}
		}
		
		public function fire(src:Tower, dst:Creep):void 
		{
			var bu:Bullet = bullets.getObject() as Bullet;
			bu.revive();
			bu.x = src.x;
			bu.y = src.y;
			bu.beamTo(dst);
			dst.health -= src.damage;
			if (dst.health <= 0)
			{
				dst.kill();
			}
		}
	}
}