package  
{
	import com.mikesoylu.fortia.*;
	import starling.core.Starling;
	import starling.display.*;
	import starling.events.*;
	
	/**
	 * @author bms
	 */
	public class DefenceMode implements fMode 
	{
		private var parent:GameState;
		
		public function init(parent:fState):void 
		{
			this.parent = parent as GameState;
		}
		
		public function update(dt:Number):void 
		{
			
		}
		
		public function activate():void 
		{
			parent.addEventListener(TouchEvent.TOUCH, touchListener);
			if (GameState.playerTeam == GameState.TEAM_B)
			{
				parent.messageOverlay.showMessage("prepare your defences for next round\n they will attack from the left");
			} else
			{
				parent.messageOverlay.showMessage("prepare your defences for next round\n they will attack from the right");
			}
		}
		
		private function touchListener(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch(parent as DisplayObject, TouchPhase.BEGAN);
			if (touch && e.target is GridCell  && (e.target as GridCell).isInRange)
			{
				var x:int = touch.target.x;
				var y:int = touch.target.y;
				SocketManager.reqNewTower(x / GameState.GRID_SIZE_IN_PIXELS - 0.5, y / GameState.GRID_SIZE_IN_PIXELS - 0.5);
			} else if (touch && e.target is Tower && (e.target as Tower).team == GameState.playerTeam)
			{
				trace("tower is active:", (e.target as Tower).active);
				parent.upgradeTowerOverlay.visible = true;
				parent.upgradeTowerOverlay.towerID = (e.target as Tower).id;
				parent.upgradeTowerOverlay.towerSpeed = (e.target as Tower).speed;
				parent.upgradeTowerOverlay.towerRange = Math.sqrt((e.target as Tower).rangeSqr)/GameState.GRID_SIZE_IN_PIXELS;
				parent.upgradeTowerOverlay.towerDamage = (e.target as Tower).damage;
			}
		}
		
		public function deactivate():void 
		{
			parent.removeEventListener(TouchEvent.TOUCH, touchListener);
			parent.upgradeTowerOverlay.visible = false;
		}
		
	}
}