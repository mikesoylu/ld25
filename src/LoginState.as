package  
{
	import feathers.themes.MetalWorksMobileTheme;
	import com.mikesoylu.fortia.*;
	import feathers.controls.TextInput;
	import flash.display.Bitmap;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	/**
	 * @author bms
	 */
	public class LoginState extends fState 
	{
		private var theme:MetalWorksMobileTheme;
		private var username:TextInput;
		private var password:TextInput;
		override public function init(e:Event):void 
		{
			super.init(e);
			theme = new MetalWorksMobileTheme(this);

			var background:Quad = new Quad(fGame.width,fGame.height,0x3C3736);
			addChild(background);
			
			var title:Image = new Image(fAssetManager.getTexture("game", "title"));
			title.pivotX = title.width / 2;
			title.y = 20;
			title.x = fGame.width / 2;
			addChild(title);
			
			username = new TextInput();
			username.setSize(80,30);
			addChild(username);
			username.validate();
			var myDate = new Date();
			var unixTime = Math.round(myDate.getTime()/1000);
			username.text = "guest" + unixTime;
			username.x = -username.width*0.5 + fGame.width / 2;
			username.y = fGame.height / 2+60;
			
			/*
			password = new TextInput();
			addChild(password);
			password.validate();
			password.height = 24;
			password.x = -password.width/2 + fGame.width / 2;
			password.y = fGame.height / 2;
			password.textEditorProperties = {displayAsPassword:true}
			*/
			
			var okButton:fButton = new fButton(fAssetManager.getTexture("game", "okButton"), "login");
			okButton.fontName = "Visitor TT1 BRK";
			okButton.fontSize = BitmapFont.NATIVE_SIZE;
			okButton.fontColor = Color.WHITE;
			okButton.pivotX = okButton.width / 2;
			okButton.pivotY = okButton.height / 2;
			okButton.x = fGame.width / 2;
			okButton.y = fGame.height / 2 + 100;
			okButton.addEventListener(Event.TRIGGERED, onSubmit);
			addChild(okButton);
			
		}
		
		private function onSubmit():void 
		{
			SocketManager.reqPlayerState(username.text);
		}
	}		
}